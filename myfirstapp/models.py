from django.db import models

# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length= 100)
    author = models.CharField(max_length= 100)
    year = models.CharField(max_length= 100)
    publisher = models.CharField(max_length= 100)
    description = models.CharField(max_length= 100)
    book_id = models.CharField(max_length= 200,null=True,blank= True)
    uploaded_by = models.CharField(max_length= 100,null= True,blank= True)
    #pdfs = models.FileField(upload_to= 'libraryproject/pdfs/')
    #Bookcover = models.ImageField(upload_to= 'libraryproject/covers/',null=True,blank= True)

    def __str__(self):
        return self.title